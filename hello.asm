;Check elixir.bootlin.com/linux for more info about function structures
SECTION .data
	ptr_text: db "Hello, world!", 0x0A	;define charbuffer
	ptr_text_size: equ $ - ptr_text 	;get the length of hello world byte sequence (sizeof()?)

SECTION .text
	global _start		;define start hook

_print_message:
	mov rax, 1		;write (see linux syscall_64.tbl)
	mov rdi, 1 		;func arg 1  fd (file descriptor) 0 = in, 1=out 2=error
	mov rsi, ptr_text 	;func arg 2 (charbuffer)
	mov rdx, ptr_text_size	;scratch register, fung arg 3 (buffer length)
	syscall 		;execute
	ret 			;return to main routine

_exit:
	push 60			;push 60 into stack (60)
	push 0			;push 0 into stack (60 0)
	pop rdi 		;error code pop 0 out of stack (60)
	pop rax 		;exit pop 60 out of stack ()
	syscall 		;execute
	ret 			;return to main routine

_start:
	call _print_message
	call _exit
