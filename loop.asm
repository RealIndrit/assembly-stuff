;Check elixir.bootlin.com/linux for more info about function structures
SECTION .data
	ptr_txt_0: db "0", 0x0A
	ptr_txt_0_len: equ $ - ptr_txt_0
	ptr_end: dq 5 ;define max for loop

SECTION .text
	global _start		;define start hook

_infinity_loop:
	call _print_message	;call print message
	jmp _infinity_loop	;jump bang to the flag beginning and repeat

_for_loop:
	inc rcx			;increment the counter register
	jmp _print_message	;execute print message
	cmp rcx, 5		;compare counter with 5
	jle _for_loop		;if less than or qeuals to 5 jump to flag
	ret			;return?

_print_message:
	mov rax, 1		;write (see linux syscall_64.tbl)
	mov rdi, 1 		;func arg 1  fd (file descriptor) 0 = in, 1=out 2=error
	mov rsi, ptr_txt_0 	;func arg 2 (charbuffer)
	mov rdx, ptr_txt_0_len	;scratch register, fung arg 3 (buffer length)
	syscall 		;execute
	ret 			;return to main routine

_exit:
	push 60			;push 60 into stack (60)
	push 0			;push 0 into stack (60 0)
	pop rdi 		;error code pop 0 out of stack (60)
	pop rax 		;exit pop 60 out of stack ()
	syscall 		;execute
	ret 			;return to main routine

_start:
	xor rcx,rcx
	jmp _for_loop
	call _exit
