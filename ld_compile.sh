while getopts i:o: flag
do
	case "${flag}" in
		i) input_file=${OPTARG};;
		o) output_file=${OPTARG};;
	esac
done

echo "Creating object file from $input_file.asm to $output_file.o"
nasm -f elf64 $input_file.asm -o $output_file.o
echo "Object file has successfully been created, linking and creating executable"
ld -o run $output_file.o -m elf_x86_64
echo "Executable has been successfully created, do ./run to run program"
