;Check elixir.bootlin.com/linux for more info about function structures
SECTION .data
	achar: db '0'		;define charbuffer

SECTION .text
	global _start		;define start hook

_print_char:
	mov rax, 1		;write (see linux syscall_64.tbl)
	mov rdi, 1 		;func arg 1  fd (file descriptor) 0 = in, 1=out 2=error
	mov rsi, achar 		;func arg 2 (charbuffer)
	mov rdx, 1		;scratch register, fung arg 3 (buffer length)
	syscall 		;execute
	ret 			;return to main routine

_exit:
	push 60			;push 60 into stack (60)
	push 0			;push 0 into stack (60 0)
	pop rdi 		;error code pop 0 out of stack (60)
	pop rax 		;exit pop 60 out of stack ()
	syscall 		;execute
	ret 			;return to main routine

_next:
	push rcx		;save counter value in stack caus call will make it loop infinitely if not done
	call _print_char	;print byte on the achar memory adress
	pop rcx			;restore counter value for loop	;compare char to base value of 0
	inc byte[achar]		;inc the value of char code by 1
	loop _next		;loop
	ret			;when rcx reaches 256 we stop

_start:
	mov rcx, 255		;define char limit
	call _next		;do the loop thingy
	call _exit		;gracefully exit according to all documentations
